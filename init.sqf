/*
init.sqf
Author:
	
	MrGWilliam

Last modified:

	07/22/2014
	
Description:

	Initalization Script run on all clients
*/
_null = [] execVM "scripts\eos\OpenMe.sqf";										// EOS (urban mission and defend AO)
_igiload = execVM "scripts\IgiLoad\IgiLoadInit.sqf";							//Igiload
_null = [] execVM "scripts\vehicle\repair\zlt_fieldrepair.sqf";					// Vehicle Field Repair


if (!isDedicated) then {
	TCB_AIS_PATH = "scripts\ais_injury\";
	{[_x] call compile preprocessFile (TCB_AIS_PATH+"init_ais.sqf")} forEach (if (isMultiplayer) then {playableUnits} else {switchableUnits});		// execute for every playable unit
};
