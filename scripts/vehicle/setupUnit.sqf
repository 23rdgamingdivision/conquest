//------------------------------ CONFIG

_unit = _this select 0;
_type = typeOf _unit;

//---------- UH-80 Ghosthawk

_ghosthawk = ["B_Heli_Transport_01_camo_F","B_Heli_Transport_01_F"];


//---------- Black camo

_blackVehicles = ["B_Heli_Light_01_F","B_Heli_Light_01_armed_F"];

//------------------------------ INITIALIZE VEHICLES

if (isNull _unit) exitWith {};

if (_type in _ghosthawk) then {
	[_unit] execVM "scripts\vehicle\animate\ghosthawk.sqf";
	_unit addAction ["<t color='#0000f6'>Ammo Drop</t>", "scripts\vehicle\drop\drop.sqf",[1],0,false,true,"","driver _target == _this"];
};

if (_type in _blackVehicles) then {
	for "_i" from 0 to 9 do { _unit setObjectTextureGlobal [_i,"#(argb,8,8,3)color(0,0,0,0.6)"]; };
};

_unit addAction ["<t color='#3f3fff'>Clear Inventory</t>","scripts\vehicle\clear\clear.sqf",[],-97,false];